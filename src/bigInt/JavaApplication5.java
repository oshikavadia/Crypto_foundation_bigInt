package bigInt;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import java.util.Scanner;

/**
 *
 * @author oshi
 */
public class JavaApplication5 {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please enter a file name to read from: ");
        String filename = keyboard.nextLine();
        try {
            Scanner myFile = new Scanner(new File(filename));
            int lineCount = 0;
            while (myFile.hasNextLine()) {
                lineCount++;
                String line = myFile.nextLine();
                String[] data = line.split(" ");
                if (data.length == 2) {
                    try {
                        BigInteger a = new BigInteger(data[0]);
                        BigInteger b = new BigInteger(data[1]);
                        if (b.compareTo(BigInteger.ZERO) == 1) {
                            BigInteger result = getModulo(a, b);
                            System.out.println("The result of " + a + " modulo " + b + " is " + result);
                        } else {
                            System.out.println("Inavlid data...");
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("An exception occurred while parsing numbers from line " + lineCount + " : " + line);
                    }
                } else if (data.length == 3) {
                    BigInteger a = new BigInteger(data[0]);
                    BigInteger b = new BigInteger(data[1]);
                    BigInteger c = new BigInteger(data[2]);
                    if (b.compareTo(BigInteger.ZERO) == 1 && c.compareTo(BigInteger.ZERO) == 1) {
                        BigInteger result = getModPow(a, c, b);
                        System.out.println("The result of " + a + "^" + b + " modulo " + c + " is " + result);
                    } else {
                        System.out.println("Invalid data...");
                    }

                } else {
                    System.out.println("Line " + lineCount + " was not formatted properly: \"" + line + "\"");
                }
            }
        } catch (FileNotFoundException fe) {
            System.out.println("That file was not found in the system, sorry.");
        }

//        System.out.println("CoPrime 1000,117 : "+isCoPrime(new BigInteger("1000"), new BigInteger("117")));
//        System.out.println(new BigInteger("5").modInverse(new BigInteger("12")));
//        System.out.println(modInverse(BigInteger.valueOf(12), BigInteger.valueOf(5)));
//        System.out.println("CoPrime 3,15 : "+isCoPrime(new BigInteger("3"), new BigInteger("15")));
//        System.out.println("CoPrime 96,321 : "+isCoPrime(new BigInteger("96"), new BigInteger("321")));
//        System.out.println("CoPrime 3696,2609 : "+isCoPrime(new BigInteger("3696"), new BigInteger("2609")));
//        System.out.println("CoPrime 4001,397 : "+isCoPrime(new BigInteger("4001"), new BigInteger("397")));

    }

    public static BigInteger getModulo(BigInteger a, BigInteger b) {

        if (a.compareTo(BigInteger.ZERO) == 0 || a.compareTo(BigInteger.ZERO) == 1) {
            BigInteger temp = a.divide(b);
            BigInteger mod = a.subtract(b.multiply(temp));
            return mod;
        } else {
            BigInteger temp = a.negate().divide(b);

            if ((temp.multiply(b)).compareTo(a.negate()) == 0) {
                return BigInteger.ZERO;
            } else {
                return b.subtract(a.negate().subtract(temp.multiply(b)));
            }
        }

    }

    public static BigInteger getModPow(BigInteger a, BigInteger b, BigInteger c) {

        List<Integer> binaryList = getBinaryList(c);
        BigInteger mod = BigInteger.ONE;
        for (int i : binaryList) {
            mod = mod.multiply(a.pow(i));
        }
        mod = getModulo(mod, b);
        return mod;

    }

    public static List<Integer> getBinaryList(BigInteger bigInt) {
        List<Integer> binaryList = new ArrayList<Integer>();
        String binaryString = bigInt.toString(2);

        int count = 0;
        for (int i = binaryString.length() - 1; i >= 0; i--) {

            if (binaryString.charAt(i) == '1') {
                binaryList.add((int) Math.pow(2, count));
            }
            count++;

        }

        return binaryList;
    }

    public static BigInteger getGcd(BigInteger m, BigInteger n) {
        if (n.compareTo(BigInteger.ZERO) == 0) {
            return m;
        } else {
            return getGcd(n, getModulo(m, n));
        }

    }

    public static boolean isCoPrime(BigInteger m, BigInteger n) {
        return getGcd(m, n).compareTo(BigInteger.ONE) == 0;
    }

//    public static BigInteger modInverse(BigInteger a, BigInteger b) {
//        BigInteger q, r, temp, x = BigInteger.ZERO, y = BigInteger.ONE, xPrime = BigInteger.ONE, yPrime = BigInteger.ZERO;
//        q = a.divide(b);
//        r = getModulo(a, b);
//        a = b;
//        b = r;
//        
//        
//        temp = x;
//        x = xPrime.subtract(q.multiply(x));
//        xPrime = temp;
//        
//        
//        temp = y;
//        y = yPrime.subtract(q.multiply(y));
//        yPrime = temp;
//        
//
//        return y;
//    }

}
